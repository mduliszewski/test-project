package com.duliszewski.empik.controller;

import com.duliszewski.empik.service.GithubDataRetrieverService;
import com.duliszewski.empik.service.dto.GithubUserInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for Github data.
 *
 * @author Michal Duliszewski
 */
@RestController
public class GithubUsersDataController {

    private final GithubDataRetrieverService retrieverService;

    @Autowired
    public GithubUsersDataController(GithubDataRetrieverService retrieverService) {
        this.retrieverService = retrieverService;
    }

    /**
     * Retrieves Github user data based on a provided login.
     *
     * @param login Github user login.
     * @return Github user data.
     */
    @GetMapping("/users/{login}")
    public GithubUserInfoDto getGithubData(@PathVariable String login) {
        return retrieverService.getGithubUserData(login);
    }
}
