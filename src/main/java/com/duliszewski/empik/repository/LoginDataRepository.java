package com.duliszewski.empik.repository;

import com.duliszewski.empik.entity.LoginData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginDataRepository extends CrudRepository<LoginData, String> {

}
