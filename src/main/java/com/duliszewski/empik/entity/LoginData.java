package com.duliszewski.empik.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Class containing login statistics.
 *
 * @author Michal Duliszewski
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginData {

    @Id
    private String login;
    private int requestCount;
}
