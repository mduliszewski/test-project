package com.duliszewski.empik.service;

import com.duliszewski.empik.entity.GithubUserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Service responsible for making api calls to external services.
 *
 * @author Michal Duliszewski
 */
@Service
@Slf4j
public class ApiCallerService {

    private final RestTemplate restTemplate;
    private final JsonParserService parserService;

    @Value("${api.github.url.users}")
    private String githubApiUrl;

    @Autowired
    public ApiCallerService(RestTemplate restTemplate, JsonParserService parserService) {
        this.restTemplate = restTemplate;
        this.parserService = parserService;
    }

    /**
     * Retrieves Github user data from Github API.
     *
     * @param login Github user login.
     * @return Github data in JSON representation
     */
    public GithubUserInfo getGithubData(String login) {
        log.info("Retrieving Github data for login = {}", login);
        String jsonResponse = getJsonResponse(constructUsersUrl(login));
        return parserService.parseGithubUsersResponse(jsonResponse);
    }

    private String getJsonResponse(String targetUrl) {
        ResponseEntity<String> response = restTemplate.getForEntity(targetUrl, String.class);
        String jsonResponse = "";

        if (response.hasBody() && response.getBody() != null) {
            jsonResponse = response.getBody();
        }
        return jsonResponse;
    }

    private String constructUsersUrl(String login) {
        return githubApiUrl + "/" + login;
    }
}
