package com.duliszewski.empik.service;

import com.duliszewski.empik.entity.GithubUserInfo;
import com.duliszewski.empik.service.dto.GithubUserInfoDto;
import com.duliszewski.empik.service.mapper.GithubUserInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service responsible for retrieving Github User data.
 *
 * @author Michal Duliszewski
 */
@Service
public class GithubDataRetrieverService {

    private final ApiCallerService apiCallerService;
    private final GithubDataCalculatorService calculatorService;
    private final GithubUserInfoMapper githubUserInfoMapper;
    private final LoginDataService loginDataService;

    @Autowired
    public GithubDataRetrieverService(ApiCallerService apiCallerService, GithubDataCalculatorService calculatorService,
                                      GithubUserInfoMapper githubUserInfoMapper, LoginDataService loginDataService) {

        this.apiCallerService = apiCallerService;
        this.calculatorService = calculatorService;
        this.githubUserInfoMapper = githubUserInfoMapper;
        this.loginDataService = loginDataService;
    }


    /**
     * Retrieves Github user data with calculations based on a provided login.
     *
     * @param login Github user login.
     * @return Github user data.
     */
    public GithubUserInfoDto getGithubUserData(String login) {
        loginDataService.handleLoginCount(login);
        GithubUserInfo userInfo = apiCallerService.getGithubData(login);
        userInfo.setCalculations(calculatorService.performCalculations(userInfo));
        return githubUserInfoMapper.toDto(userInfo);
    }

}
