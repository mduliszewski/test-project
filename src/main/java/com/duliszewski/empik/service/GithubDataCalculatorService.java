package com.duliszewski.empik.service;

import com.duliszewski.empik.entity.GithubUserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Service responsible for calculations associated with Github data.
 *
 * @author Michal Duliszewski
 */
@Service
@Slf4j
class GithubDataCalculatorService {

    /**
     * Performs required calculations.
     *
     * @param userInfo Required user info for calculations
     * @return a result of calculations
     */
    public double performCalculations(GithubUserInfo userInfo) {
        log.info("Performing calculations for login = {}", userInfo.getLogin());
        double result = 0;
        if (userInfo.getFollowersCount() != 0) {
            result = 6 / userInfo.getFollowersCount() * (2 + userInfo.getPublicReposCount());
        }
        return result;
    }
}
