package com.duliszewski.empik.service;

import java.util.Optional;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duliszewski.empik.entity.LoginData;
import com.duliszewski.empik.repository.LoginDataRepository;

/**
 * Service responsible for LoginData handling.
 *
 * @author Michal Duliszewski
 */
@Service
@Slf4j
public class LoginDataService {

    private final LoginDataRepository loginDataRepository;

    @Autowired
    public LoginDataService(LoginDataRepository loginDataRepository) {
        this.loginDataRepository = loginDataRepository;
    }

    /**
     * Handles logic associated with counting the number of request for the https://api.github.com/users/login endpoint.
     *
     * @param login Github user login
     * @return login data
     */
    public LoginData handleLoginCount(String login) {
        Optional<LoginData> loginDataOptional = loginDataRepository.findById(login);

        LoginData loginData = new LoginData();
        if (loginDataOptional.isPresent()) {
            loginData = loginDataOptional.get();
            int requestCount = loginData.getRequestCount() + 1;
            loginData.setRequestCount(requestCount);
            log.info("Got already persisted login = {}, increasing request count to {}", login, requestCount);
        } else {
            loginData.setLogin(login);
            int requestCount = 1;
            loginData.setRequestCount(requestCount);
            log.info("Got new login = {}, count = {}", login, requestCount);
        }
        return loginDataRepository.save(loginData);
    }
}
