package com.duliszewski.empik.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * DTO object containing required github user data.
 *
 * @author Michal Duliszewski
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GithubUserInfoDto {

    private Long id;
    private String login;
    private String name;
    private String type;
    private String avatarUrl;
    private LocalDateTime createdAt;
    private double calculations;
}
