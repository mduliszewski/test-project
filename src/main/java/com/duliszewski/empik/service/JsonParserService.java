package com.duliszewski.empik.service;

import com.duliszewski.empik.entity.GithubUserInfo;
import com.duliszewski.empik.exception.JsonParserServiceException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Service responsible for parsing of JSON responses.
 *
 * @author Michal Duliszewski
 */
@Service
@Slf4j
public class JsonParserService {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String GITHUB_PARSING_ERROR = "Error parsing Github json response.";

    /**
     * Parses Github user info JSON response.
     *
     * @param jsonResponse Github JSON response
     * @return Information about Github user
     */
    public GithubUserInfo parseGithubUsersResponse(String jsonResponse) {
        try {
            return mapper.readValue(jsonResponse, GithubUserInfo.class);
        } catch (JsonProcessingException e) {
            log.error(GITHUB_PARSING_ERROR, e);
            throw new JsonParserServiceException(GITHUB_PARSING_ERROR);
        }
    }
}
