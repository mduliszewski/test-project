package com.duliszewski.empik.service.mapper;

import com.duliszewski.empik.entity.GithubUserInfo;
import com.duliszewski.empik.service.dto.GithubUserInfoDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GithubUserInfoMapper {

    GithubUserInfoDto toDto(GithubUserInfo githubUserInfo);
}
