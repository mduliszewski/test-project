package com.duliszewski.empik.exception;

public class JsonParserServiceException extends RuntimeException {

    public JsonParserServiceException() {
        super();
    }

    public JsonParserServiceException(String message) {
        super(message);
    }

    public JsonParserServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
