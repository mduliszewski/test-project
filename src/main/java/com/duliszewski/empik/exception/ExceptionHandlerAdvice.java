package com.duliszewski.empik.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;


@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity<String> handleException(HttpStatusCodeException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }

    @ExceptionHandler(JsonParserServiceException.class)
    public ResponseEntity<String> handleException(JsonParserServiceException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(exception.getMessage());
    }
}
