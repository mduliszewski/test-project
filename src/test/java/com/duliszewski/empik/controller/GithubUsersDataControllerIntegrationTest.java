package com.duliszewski.empik.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.duliszewski.empik.service.dto.GithubUserInfoDto;

import java.time.LocalDateTime;

@SpringBootTest
class GithubUsersDataControllerIntegrationTest {

    private static final String LOGIN = "octocat";

    @Autowired
    private GithubUsersDataController dataController;

    @Test
    void shouldReturnProperGithubInfoDataWithCalculations() {
        //given-when
        GithubUserInfoDto githubInfo = dataController.getGithubData(LOGIN);

        //then
        assertThat(githubInfo).isNotNull();
        assertThat(githubInfo.getId()).isEqualTo(583231L);
        assertThat(githubInfo.getType()).isEqualTo("User");
        assertThat(githubInfo.getCalculations()).isPositive();
        assertThat(githubInfo.getCreatedAt()).isEqualTo(LocalDateTime.of(2011, 1, 25, 18, 44, 36));
    }

}
