package com.duliszewski.empik.service;


import com.duliszewski.empik.entity.GithubUserInfo;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class GithubDataCalculatorServiceTest {

    private static final String LOGIN = "login";
    private static final GithubDataCalculatorService calculatorService = new GithubDataCalculatorService();

    @Test
    void shouldPerformCalculationsBasedOnUserData() {
        //given
        GithubUserInfo userInfo = GithubUserInfo.builder()
                .login(LOGIN)
                .followersCount(1)
                .publicReposCount(14)
                .build();

        //when
        double result = calculatorService.performCalculations(userInfo);

        //then
        assertThat(result).isEqualTo(96);
    }

    @Test
    void shouldPerformCalculationsBasedOnUserDataReturnsZeroAsFollowersCountIsZero() {
        //given
        GithubUserInfo userInfo = GithubUserInfo.builder()
                .login(LOGIN)
                .followersCount(0)
                .publicReposCount(14)
                .build();

        //when
        double result = calculatorService.performCalculations(userInfo);

        //then
        assertThat(result).isZero();
    }
}
