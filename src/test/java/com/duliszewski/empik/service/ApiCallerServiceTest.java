package com.duliszewski.empik.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import com.duliszewski.empik.entity.GithubUserInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
class ApiCallerServiceTest {

    private static final String LOGIN = "LOGIN";

    @Test
    void shouldGetGithubData(@Mock RestTemplate restTemplate, @Mock JsonParserService parserService) {
        //given
        when(parserService.parseGithubUsersResponse(anyString())).thenReturn(GithubUserInfo.builder()
                .login(LOGIN)
                .build());

        when(restTemplate.getForEntity(anyString(), eq(String.class))).thenReturn(ResponseEntity.ok(""));

        ApiCallerService apiCallerService = new ApiCallerService(restTemplate, parserService);
        //when
        GithubUserInfo result = apiCallerService.getGithubData(LOGIN);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getLogin()).isEqualTo(LOGIN);
    }

}
