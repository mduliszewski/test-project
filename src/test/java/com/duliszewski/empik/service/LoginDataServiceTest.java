package com.duliszewski.empik.service;

import com.duliszewski.empik.entity.LoginData;
import com.duliszewski.empik.repository.LoginDataRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LoginDataServiceTest {

    private static final String LOGIN = "LOGIN";

    @Test
    void shouldReturnNewlyCreatedLoginData(@Mock LoginDataRepository loginDataRepository) {
        //given
        LoginData loginThatIsPersisted = new LoginData(LOGIN, 1);

        when(loginDataRepository.findById(LOGIN)).thenReturn(Optional.empty());
        when(loginDataRepository.save(loginThatIsPersisted)).thenReturn(loginThatIsPersisted);

        LoginDataService loginDataService = new LoginDataService(loginDataRepository);

        //when
        LoginData result = loginDataService.handleLoginCount(LOGIN);

        //then
        assertThat(result.getLogin()).isEqualTo(LOGIN);
        assertThat(result.getRequestCount()).isEqualTo(1);
    }

    @Test
    void shouldReturnLoginDataWithIncreasedCount(@Mock LoginDataRepository loginDataRepository) {
        //given
        LoginData loginData = new LoginData(LOGIN, 7);
        LoginData loginThatIsPersisted = new LoginData(LOGIN, 8);

        when(loginDataRepository.findById(LOGIN)).thenReturn(Optional.of(loginData));
        when(loginDataRepository.save(loginThatIsPersisted)).thenReturn(loginThatIsPersisted);

        LoginDataService loginDataService = new LoginDataService(loginDataRepository);

        //when
        LoginData result = loginDataService.handleLoginCount(LOGIN);

        //then
        assertThat(result.getLogin()).isEqualTo(LOGIN);
        assertThat(result.getRequestCount()).isEqualTo(8);
    }

}
