package com.duliszewski.empik.service;

import com.duliszewski.empik.entity.GithubUserInfo;
import com.duliszewski.empik.exception.JsonParserServiceException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtendWith(MockitoExtension.class)
class JsonParserServiceTest {

    private static final JsonParserService parserService = new JsonParserService();

    @Test
    void shouldParseGithubUsersResponse() {
        //given
        String jsonResponse = "{\n" +
                "  \"login\": \"myhduck\",\n" +
                "  \"id\": 1555350,\n" +
                "  \"node_id\": \"MDQ6VXNlcjE1NTUzNTA=\",\n" +
                "  \"avatar_url\": \"https://avatars2.githubusercontent.com/u/1555350?v=4\",\n" +
                "  \"gravatar_id\": \"\",\n" +
                "  \"url\": \"https://api.github.com/users/myhduck\",\n" +
                "  \"html_url\": \"https://github.com/myhduck\",\n" +
                "  \"followers_url\": \"https://api.github.com/users/myhduck/followers\",\n" +
                "  \"following_url\": \"https://api.github.com/users/myhduck/following{/other_user}\",\n" +
                "  \"gists_url\": \"https://api.github.com/users/myhduck/gists{/gist_id}\",\n" +
                "  \"starred_url\": \"https://api.github.com/users/myhduck/starred{/owner}{/repo}\",\n" +
                "  \"subscriptions_url\": \"https://api.github.com/users/myhduck/subscriptions\",\n" +
                "  \"organizations_url\": \"https://api.github.com/users/myhduck/orgs\",\n" +
                "  \"repos_url\": \"https://api.github.com/users/myhduck/repos\",\n" +
                "  \"events_url\": \"https://api.github.com/users/myhduck/events{/privacy}\",\n" +
                "  \"received_events_url\": \"https://api.github.com/users/myhduck/received_events\",\n" +
                "  \"type\": \"User\",\n" +
                "  \"site_admin\": false,\n" +
                "  \"name\": \"YOHAN MOON\",\n" +
                "  \"company\": \"home\",\n" +
                "  \"blog\": \"\",\n" +
                "  \"location\": \"korea\",\n" +
                "  \"email\": null,\n" +
                "  \"hireable\": null,\n" +
                "  \"bio\": null,\n" +
                "  \"twitter_username\": null,\n" +
                "  \"public_repos\": 1,\n" +
                "  \"public_gists\": 0,\n" +
                "  \"followers\": 2,\n" +
                "  \"following\": 2,\n" +
                "  \"created_at\": \"2012-03-20T05:15:06Z\",\n" +
                "  \"updated_at\": \"2018-09-10T01:55:51Z\"\n" +
                "}\n";

        //when
        GithubUserInfo result = parserService.parseGithubUsersResponse(jsonResponse);

        //then
        assertThat(result.getLogin()).isEqualTo("myhduck");
        assertThat(result.getId()).isEqualTo(1555350);
        assertThat(result.getName()).isEqualTo("YOHAN MOON");
        assertThat(result.getCreatedAt()).isEqualTo(LocalDateTime.of(2012, 3, 20, 5, 15, 6));
    }

    @Test
    void shouldNotParseGithubUsersResponseBecauseOfImproperJson() {
        //given
        String invalidJson = "invalidJson";

        //when-then
        assertThatExceptionOfType(JsonParserServiceException.class).isThrownBy(() -> parserService.parseGithubUsersResponse(invalidJson))
                .withMessage("Error parsing Github json response.");
    }
}
