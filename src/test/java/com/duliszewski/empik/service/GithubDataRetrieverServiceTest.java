package com.duliszewski.empik.service;

import com.duliszewski.empik.entity.GithubUserInfo;
import com.duliszewski.empik.service.dto.GithubUserInfoDto;
import com.duliszewski.empik.service.mapper.GithubUserInfoMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GithubDataRetrieverServiceTest {

    private static final String LOGIN = "login";

    @Test
    void shouldGetGithubUserData(@Mock ApiCallerService apiCallerService,
                                 @Mock GithubUserInfoMapper mapper,
                                 @Mock LoginDataService loginDataService) {

        //given
        GithubUserInfo userInfo = GithubUserInfo.builder()
                .login(LOGIN)
                .followersCount(1)
                .publicReposCount(14)
                .build();

        when(apiCallerService.getGithubData(LOGIN)).thenReturn(userInfo);
        GithubUserInfoDto userInfoDto = GithubUserInfoDto.builder().login(LOGIN).build();
        when(mapper.toDto(userInfo)).thenReturn(userInfoDto);

        GithubDataRetrieverService retrieverService = new GithubDataRetrieverService(apiCallerService, new GithubDataCalculatorService(), mapper, loginDataService);

        //when
        GithubUserInfoDto result = retrieverService.getGithubUserData(LOGIN);

        //then
        assertThat(result).isNotNull().isEqualToComparingFieldByField(userInfoDto);
    }

}
